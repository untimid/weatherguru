### Documentation

1. Download project or clone with "git clone" via bash
2. Run console in project root folder
3. In console: "npm install" , after installation finished, "npm start"
4. Have fun!

### Folders and files

- public - html boilerplate, icons etc.
- src - source code
- - actions - action "types.js" and action creators
- - layout - default layout component with child components
- - locale - localization packages
- - modules - app modules - 2 pages: weather and search history
- - reducers - reducers setup
