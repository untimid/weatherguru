import React from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { updateLocales } from "../actions";

const useStyles = makeStyles(theme => ({
  select: {
    marginLeft: theme.spacing(2),
  },
}));

const LanguageSelect = ({ locale, updateLocales }) => {
  const classes = useStyles();

  return (
    <Select
      className={classes.select}
      labelId="demo-simple-select-label"
      id="language-select"
      value={locale}
      onChange={event => {
        console.log(event);
        updateLocales(event.target.value);
      }}
    >
      <MenuItem value={"en-US"}>in-english</MenuItem>
      <MenuItem value={"ru-RU"}>по-русский</MenuItem>
    </Select>
  );
};

const mapStateToProps = ({ locale }) => {
  return { locale };
};
const mapDispatchToProps = { updateLocales };

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSelect);
