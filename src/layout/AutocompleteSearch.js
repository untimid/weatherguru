/* eslint-disable no-use-before-define */
import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { fetchWeather } from "../actions";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import SearchIcon from "@material-ui/icons/Search";
import history from "../history";
/*
Autocomplete search can be perfectly done with google maps API,
 but for now predefined arrays of cities for both locales will be in use
*/
import citiesData from "../locale/citiesData.json";

const useStyles = makeStyles(theme => ({
  wrapper: {
    display: "flex",
    flexWrap: "noWrap",
    justifyContent: "center",
    width: "100%",
  },
  input: {
    flexGrow: 2,
    maxWidth: 250,
    marginRight: theme.spacing(1),
    display: "flex",
    alignContent: "center",
  },
  searchButton: {
    marginTop: theme.spacing(1) + 5,
    marginBottom: theme.spacing(1) + 2,
  },
  textField: {
    flexGrow: 1,
    alignSelf: "center",
  },
}));

const AutocompleteSearch = ({ fetchWeather, locale }) => {
  const [cityValue, setCityValue] = useState(null);
  const classes = useStyles();
  const handleSearch = () => {
    if (cityValue) {
      fetchWeather(cityValue, locale);
      history.push("/");
    }
  };

  return (
    <div className={classes.wrapper}>
      <Autocomplete
        className={classes.input}
        id="main-search"
        options={citiesData[locale]}
        getOptionLabel={option => option.title}
        noOptionsText={<FormattedMessage id="search.noOptions" defaultMessage="нет вариантов" />}
        value={cityValue}
        onChange={(event, newCityValue) => {
          setCityValue(newCityValue);
        }}
        renderInput={params => (
          <TextField
            {...params}
            margin="normal"
            size="small"
            className={classes.textField}
            // label={<FormattedMessage id="search.label" defaultMessage="начните вводить город" />}
          />
        )}
      />
      <Button
        disabled={cityValue ? false : true}
        variant="contained"
        color={"secondary"}
        startIcon={<SearchIcon />}
        className={classes.searchButton}
        onClick={handleSearch}
      >
        <FormattedMessage id="search.button" defaultMessage="найти!" />
      </Button>
    </div>
  );
};

const mapStateToProps = ({ locale }) => {
  return { locale };
};

const mapDispatchToProps = { fetchWeather };

export default connect(mapStateToProps, mapDispatchToProps)(AutocompleteSearch);
