import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Container from "@material-ui/core/Container";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import MenuItems from "./MenuItems";
import AutocompleteSearch from "./AutocompleteSearch";
import LanguageSelect from "./LanguageSelect";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
  },
  toolbar: {
    display: "flex",
    alignItems: "space-between",
  },
  appBar: {},
  title: {
    flexGrow: 1,
  },
  avatar: {
    alignSelf: "center",
    paddingBottom: theme.spacing(1),
  },
  logo: {
    display: "flex",
    alignItems: "center",
    marginRight: theme.spacing(2),
  },
  menu: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
  search: {
    display: "flex",
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  fixedHeight: {
    height: 240,
  },
}));

const DefaultLayout = ({ children }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="absolute" color="transparent" className={classes.appBar}>
        <Container>
          <Toolbar>
            <Grid container justifyContent="center" alignItems="center">
              <Grid item xs={2} container alignItems="baseline">
                <Avatar
                  alt="WeatherGuru logo"
                  src="/ms-icon-310x310.png"
                  className={classes.avatar}
                />
                <Typography
                  component="h1"
                  variant="h6"
                  color="inherit"
                  noWrap
                  className={classes.title}
                >
                  WeatherGuru
                </Typography>
              </Grid>
              <Grid item xs>
                <AutocompleteSearch />
              </Grid>
              <Grid item xs={4}>
                <MenuItems />
                <LanguageSelect />
              </Grid>
            </Grid>
          </Toolbar>
        </Container>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container className={classes.container}>{children}</Container>
      </main>
    </div>
  );
};

export default DefaultLayout;
