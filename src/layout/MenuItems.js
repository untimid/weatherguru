import React from "react";
import { Link as RouterLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import routes from "../modules/routes";
import { useHistory } from "react-router-dom";

const MenuItems = () => {
  const history = useHistory();

  const isSelected = path => history.location.pathname === path;

  return (
    <>
      {routes.map(route => {
        return (
          <Button
            key={route.path}
            color={isSelected(route.path) ? "secondary" : "default"}
            startIcon={<route.icon />}
            component={RouterLink}
            to={route.path}
          >
            {route.name}
          </Button>
        );
      })}
    </>
  );
};

export default MenuItems;
