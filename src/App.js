import React from "react";
import AppTree from "./AppTree";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import { createMuiTheme } from "@material-ui/core/styles";
import { MuiThemeProvider } from "@material-ui/core/styles";
import reducers from "./reducers";

// Setup theme for material-ui
const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#6cbeff",
      main: "#2f93f1",
      dark: "#1d51ac",
      contrastText: "#fff",
      primaryText: "#000",
    },
    secondary: {
      light: "#ffdf54",
      main: "#fcc931",
      dark: "#ac781d",
      contrastText: "#000",
    },
    redAlert: {
      light: "#ff7961",
      main: "#f44336",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});

//setup store, add middleware
const store = createStore(reducers, {}, applyMiddleware(reduxThunk));

const App = () => {
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <AppTree />
      </MuiThemeProvider>
    </Provider>
  );
};

export default App;
