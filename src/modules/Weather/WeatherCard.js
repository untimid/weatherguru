import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import CardHeader from "@material-ui/core/CardHeader";
import { FormattedDate, FormattedTime, FormattedMessage } from "react-intl";

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 275,
    height: "100%",
  },
  title: {
    fontSize: 16,
    wordWrap: "normal",
  },
  content: {
    padding: theme.spacing(2),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

const WeatherCard = ({ weather, isCurrent }) => {
  console.log(weather.dt);
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar
            className={classes.large}
            alt={weather.weather[0].description}
            src={`http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`}
          />
        }
        title={
          <FormattedDate
            value={new Date(weather.dt * 1000)}
            year="numeric"
            month="long"
            day="2-digit"
          />
        }
        subheader={
          <Typography className={classes.title}>{weather.weather[0].description}</Typography>
        }
      />
      <CardContent className={classes.content}>
        <Typography gutterBottom>
          <FormattedMessage id="card.sunrise" defaultMessage="Восход" />:{" "}
          <FormattedTime value={new Date(weather.sunrise * 1000)} />
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.sunset" defaultMessage="Закат" />:{" "}
          <FormattedTime value={new Date(weather.sunset * 1000)} />
        </Typography>
        {isCurrent && (
          <>
            <Typography gutterBottom>
              <FormattedMessage id="card.temperatureDay" defaultMessage="Температура днем" />:{" "}
              {weather.temp}
            </Typography>
            <Typography gutterBottom>
              <FormattedMessage id="card.feelsLike" defaultMessage="Ощущается как" />:{" "}
              {weather.feels_like}
            </Typography>
          </>
        )}
        {!isCurrent && (
          <>
            <Typography gutterBottom>
              <FormattedMessage id="card.temperatureDay" defaultMessage="Температура днем" />:{" "}
              {weather.temp.day}
            </Typography>
            <Typography gutterBottom>
              <FormattedMessage id="card.feelsLike" defaultMessage="Ощущается как" />:{" "}
              {weather.feels_like.day}
            </Typography>
            <Typography gutterBottom>
              <FormattedMessage id="card.temperatureNight" defaultMessage="Температура ночью" />:{" "}
              {weather.temp.night}
            </Typography>
            <Typography gutterBottom>
              <FormattedMessage id="card.feelsLike" defaultMessage="Ощущается как" />:{" "}
              {weather.feels_like.night}
            </Typography>
          </>
        )}
        <Typography gutterBottom>
          <FormattedMessage id="card.pressure" defaultMessage="Давление" />: {weather.pressure}
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.humidity" defaultMessage="Влажность" />: {weather.humidity}
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.windSpeed" defaultMessage="Скорость ветра" />:{" "}
          {weather.wind_speed}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default WeatherCard;
