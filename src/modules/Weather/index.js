import React from "react";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import WeatherCard from "./WeatherCard";

const Weather = ({ weather, weatherError }) => {
  const weatherData = weather.data;
  if (!weatherData.current) {
    if (!weatherData.error) {
      return (
        <Grid container spacing={4} justify="center">
          <Typography>
            <FormattedMessage
              id="weather.placeholder"
              defaultMessage="Начните вводить город, выберите вариант из списка и нажмите НАЙТИ!"
            />
          </Typography>
        </Grid>
      );
    } else {
      return (
        <Grid container spacing={4} justify="center">
          <Typography status="error">
            <FormattedMessage
              id="weather.error"
              defaultMessage="Что-то пошло не так. Попробуйте снова позже."
            />
          </Typography>
        </Grid>
      );
    }
  }

  const currentWeather = { ...weatherData.current };
  const dailyWeather = weatherData.daily;
  return (
    <Grid container spacing={2} justify="center" alignItems="stretch">
      {dailyWeather.slice(0, 4).map(weatherDay => {
        return (
          <Grid item xs key={weatherDay.dt}>
            <WeatherCard weather={weatherDay} />
          </Grid>
        );
      })}
    </Grid>
  );
};

const mapStateToProps = ({ weather, weatherError }) => {
  return { weather, weatherError };
};

export default connect(mapStateToProps, null)(Weather);
