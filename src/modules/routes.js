/*
Here we import all modules and export them as routes array with each module settings.
This is how global app layout is separated from inner modules.
In this app structure to add module, create it and add to this combine file,
You do not need to add something to menu file of edit layouts
*/
import React from "react";

// Importing modules
import Weather from "./Weather";
import RequestHistory from "./RequestsHistory";
import { FormattedMessage } from "react-intl";
// Import Icons for left menu
import { WbSunny, History } from "@material-ui/icons";

const routes = [
  /*
  {
    path: "/", // path for Router
    exact: true, // if exact
    component: TaskManager, // module component
    icon: FormatListBulleted, // menu icon
    order: 1, // Order in side menu
    name: "Task manager", // Menu title
  },
  */
  {
    path: "/",
    exact: true,
    component: Weather,
    icon: WbSunny,
    order: 0,
    name: <FormattedMessage id="menu.weather" defaultMessage="Погода" />,
  },
  {
    path: "/history",
    component: RequestHistory,
    icon: History,
    order: 1,
    name: <FormattedMessage id="menu.history" defaultMessage="история поиска" />,
  },
];

export default routes;
