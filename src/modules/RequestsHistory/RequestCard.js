import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { FormattedDate, FormattedTime, FormattedMessage } from "react-intl";
import citiesData from "../../locale/citiesData.json";

const useStyles = makeStyles(theme => ({
  root: {
    minWidth: 275,
  },
  title: {
    fontSize: 20,
  },
  content: {
    padding: theme.spacing(1),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

const RequestCard = ({ request, locale }) => {
  const classes = useStyles();

  const renderCityTitle = () => {
    return citiesData[locale].find(city => city.lat === request.lat && city.lon === request.lon)
      .title;
  };
  return (
    <Card className={classes.root}>
      <CardContent className={classes.content}>
        <Typography gutterBottom>
          <FormattedMessage id="card.city" defaultMessage="Город" />: {renderCityTitle()}
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.requestDate" defaultMessage="Дата запроса" />:
          <FormattedDate value={new Date(request.created)} />/
          <FormattedTime value={new Date(request.created)} />
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.temperatureDay" defaultMessage="Температура днем" />:{" "}
          {request.temp}
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.pressure" defaultMessage="Давление" />: {request.pressure}
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.humidity" defaultMessage="Влажность" />: {request.humidity}
        </Typography>
        <Typography gutterBottom>
          <FormattedMessage id="card.windSpeed" defaultMessage="Скорость верта" />:{" "}
          {request.wind_speed}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default RequestCard;
