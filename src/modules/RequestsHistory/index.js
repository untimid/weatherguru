import React, { useState } from "react";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import Pagination from "@material-ui/lab/Pagination";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RequestCard from "./RequestCard";

const compare = (a, b, reverse) => {
  if (!reverse) {
    return a.created > b.created ? 1 : b.created > a.created ? -1 : 0;
  } else {
    return a.created > b.created ? -1 : b.created > a.created ? 1 : 0;
  }
};

const RequestsHistory = ({ weather, locale }) => {
  const [page, setPage] = useState(1);
  const [eldersFirst, setEldersFirst] = useState(false);
  const history = weather.history;

  history.sort((a, b) => compare(a, b, eldersFirst));

  const ITEMS_ON_PAGE = 4;

  const RenderPagination = () => {
    const pagesCount = Math.ceil(history.length / ITEMS_ON_PAGE);
    return (
      <Pagination
        variant="outlined"
        page={page}
        count={pagesCount}
        onChange={(event, page) => {
          setPage(page);
        }}
      />
    );
  };

  return (
    <Grid container spacing={2} justify="center">
      <Grid item lg={12} container justify="center">
        <FormControlLabel
          control={
            <Switch
              checked={eldersFirst}
              onChange={event => setEldersFirst(event.target.checked)}
              name="elderFirstChecker"
              color="secondary"
            />
          }
          label={<FormattedMessage id="switch.eldersFirst" defaultMessage="Старые сначала" />}
        />
      </Grid>
      <Divider style={{ width: "100%", marginTop: 10, marginBottom: 10 }} />
      {[...history].slice(page - 1, page + 3).map(request => (
        <Grid item key={request.created}>
          <RequestCard request={request} locale={locale} />
        </Grid>
      ))}
      <Divider style={{ width: "100%", marginTop: 10, marginBottom: 10 }} />
      <RenderPagination />
    </Grid>
  );
};

const mapStateToProps = ({ weather, locale }) => {
  return { weather, locale };
};

export default connect(mapStateToProps, null)(RequestsHistory);
