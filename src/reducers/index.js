import { combineReducers } from "redux";
import weatherReducer from "./weatherReducer";
import localesReducer from "./localesReducer";

export default combineReducers({
  weather: weatherReducer,
  locale: localesReducer,
});
