import { UPDATE_LOCALES } from "../actions/types";

const initialState = navigator.language === "en-US" ? "en-US" : "ru-RU";
export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_LOCALES:
      return action.payload;
    default:
      return state;
  }
};
