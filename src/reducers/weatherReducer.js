import { FETCH_WEATHER, WEATHER_ERROR } from "../actions/types";

export default (state = { data: {}, history: [], error: "" }, action) => {
  switch (action.type) {
    case FETCH_WEATHER:
      return {
        ...state,
        data: action.payload.data,
        history: [...state.history, action.payload.updateItem],
      };
    case WEATHER_ERROR:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
