export const FETCH_WEATHER = "fetch_weather";
export const WEATHER_ERROR = "weather_error";
export const SET_LOADING = "set_loading";
export const UPDATE_LOCALES = "update_locales";
