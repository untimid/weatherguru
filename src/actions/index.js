import { FETCH_WEATHER, WEATHER_ERROR, SET_LOADING, UPDATE_LOCALES } from "./types";
import axios from "axios";

// fetch weather action
export const fetchWeather = (cityValue, locale) => async dispatch => {
  try {
    dispatch({ type: SET_LOADING, payload: true });
    const { lat, lon } = cityValue;
    /*
        it's bad practice to keep api keys that open way,
        especially for production. Our app is not for production, so i do as i do
        */
    const APIkey = "4fb405b3c08dc709048012bf3df9e8f1";
    const lang = locale === "en-US" ? "en" : "ru";
    const units = "metric";
    // create api call url
    const apiCall = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely&appid=${APIkey}&lang=${lang}&units=${units}`;
    // make request
    const response = await axios.get(apiCall);
    // simplify data, which will be push to history in reducer
    const currentWeather = response.data.current;
    const updateItem = {
      lat: cityValue.lat,
      lon: cityValue.lon,
      created: Date.now(),
      temp: currentWeather.temp,
      pressure: currentWeather.pressure,
      humidity: currentWeather.humidity,
      wind_speed: currentWeather.wind_speed,
    };

    // dispatch fetch_weather action
    dispatch({ type: FETCH_WEATHER, payload: { data: response.data, updateItem } });
    dispatch({ type: SET_LOADING, payload: false });
  } catch (err) {
    dispatch({
      type: WEATHER_ERROR,
      payload: "Error while fetching weather",
    });
  }
};

// update locale

export const updateLocales = value => dispatch => {
  dispatch({ type: UPDATE_LOCALES, payload: value });
};
