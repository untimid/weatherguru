import React from "react";
import { connect } from "react-redux";
import { Router, Route } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import routes from "./modules/routes";
import DefaultLayout from "./layout/DefaultLayout";
import { IntlProvider } from "react-intl";
import history from "./history";

import en from "./locale/en-US.json";
import ru from "./locale/ru-RU.json";

const AppTree = ({ locale }) => {
  const localization = locale === "en-US" ? en : ru;
  return (
    <>
      <CssBaseline />
      <Router history={history}>
        <IntlProvider key={locale} locale={locale} messages={localization}>
          <DefaultLayout>
            {routes.map((route, index) => {
              return (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={props => {
                    return <route.component {...props} />;
                  }}
                />
              );
            })}
          </DefaultLayout>
        </IntlProvider>
      </Router>
    </>
  );
};
const mapStateToProps = ({ locale }) => {
  return { locale };
};

export default connect(mapStateToProps, null)(AppTree);
